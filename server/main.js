import app from './app';
import errorHandler from './middlewares/errorHandler';
import authRoute from './routes/auth-route';
import postRoute from './routes/post-route';

app.use('/api', authRoute);
app.use('/api', postRoute);
app.use('*', (req, res, next) => {
  next({
    status: 404,
    message: 'endpoint not found'
  });
})

app.use(errorHandler());

export default app;