import express from 'express';

import * as PostController from '../controllers/post-controller';
import checkToken from '../middlewares/checkToken';

const router = express.Router();

router.get('/posts', PostController.fetchAll);
router.get('/posts/:url', PostController.getPostByUrl);
router.post('/posts', checkToken, PostController.createPost);
router.put('/posts', checkToken, PostController.updatePost);
router.delete('/posts/:url', checkToken, PostController.deletePostByUrl);

export default router;