import mongoose from 'mongoose'
import bluebird from 'bluebird';
import User from '../models/user'
import { database } from '../config';

mongoose.Promise = bluebird;

mongoose.connect(database.name, async err => {
  if (err) return console.log(err);

  console.log(`Mongodb connected to "${database.name}"`);

  const user = {
    login: 'admin',
    password: 'admin',
  };

  try {
    console.log(await User.create(user));
    process.exit(0)
  } catch(e) {
    throw e;
    process.exit(1)
  }
});