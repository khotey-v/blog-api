import { ADMIN_ROLE } from '../constants/roles'

export default (req, res, next) => {
  const { token: { role } } = req;

  if (role !== ADMIN_ROLE) {
    return next({
      status: 403,
      message: 'Forbidden!'
    });
  }

  next();
}