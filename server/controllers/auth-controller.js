import jwt from 'jsonwebtoken';

import User from '../models/user';
import { secret } from '../config';

export const signup = async (req, res, next) => {
  const credentials = req.body;
  let user;

  try {
    user = await User.create(credentials);
  } catch ({ message }) {
    return next({
      status: 400,
      message
    });
  }

  res.json(user);
};

export const signin = async (req, res, next) => {
  const { login, password } = req.body;
  console.log(req.body)

  const user = await User.findOne({ login });

  if (!user) {
    return next({
      status: 400,
      message: 'User not found.'
    });
  }

  try {
    await user.comparePasswords(password);
  } catch (e) {
    return next({
      status: 400,
      message: 'Bad Credentials.'
    });
  }

  const token = jwt.sign({ _id: user._id }, secret);

  res.json(token);
};