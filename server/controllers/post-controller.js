import Post from '../models/post';

export const createPost = async (req, res, next) => {
  const postData = req.body;
  let post;

  try {
    post = await Post.create(postData);
  } catch ({ message }) {
    console.log(message)
    return next({
      status: 400,
      message,
    });
  }

  res.json(post);
}

export const updatePost = async (req, res, next) => {
  const postData = req.body;
  let post;

  try {
    post = await Post.findOneAndUpdate({ _id: postData._id }, { $set: postData }, { 'new': true });
  } catch ({ message }) {
    return next({
      status: 400,
      message,
    });
  }

  res.json(post);
}

export const fetchAll = async (req, res, next) => {
  let posts;
  let query = req.query;

  try {
    posts = await Post.find(query);
  } catch ({ message }) {
    return next({
      status: 400,
      message,
    });
  }

  res.json({ data: posts });
}

export const getPostByUrl = async (req, res, next) => {
  const { url } = req.params;
  let post;

  try {
    post = await Post.findOne({ url });
  } catch ({ message }) {
    return next({
      status: 400,
      message,
    });
  }

  res.json(post);
}

export const deletePostByUrl = async (req, res, next) => {
  const { url } = req.params;
  let post;

  try {
    post = await Post.findOne({ url });
  } catch ({ message }) {
    return next({
      status: 400,
      message,
    });
  }

  if (!post) {
    return next({
      status: 404,
      message: `Post with url "${url}" not found`,
    });
  }

  await post.remove();
  res.json(post);
}