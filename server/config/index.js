import ENV, { isProd, isDev } from './env'
import PORT from './port'
import database from './database'

const secret = 'secret';

export {
  isDev,
  isProd,
  ENV,
  PORT,
  database,
  secret,
}

export default {
  isDev,
  isProd,
  ENV,
  PORT,
  database,
  secret
}
