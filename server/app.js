import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import { isDev } from './config';

const app = express();

if (isDev) {
  app.use(morgan('tiny'));
}

app.use(cors({
  origin: 'http://localhost:3000',
  credentials: true,
}));
app.use(compression());
app.use(cookieParser())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

export default app;