import 'babel-polyfill';
import mongoose from 'mongoose';
import bluebird from 'bluebird';
import app from './main';
import {
  database,
  PORT,
} from './config';

app.listen(PORT, err => {
  if (err) return console.log(err);

  console.log(`Server running on port: ${PORT}`);
});

mongoose.Promise = bluebird;

mongoose.connect(database.name, err => {
  if (err) return console.log(err);

  console.log(`Mongodb connected to "${database.name}"`);
});