import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcrypt'

const PostShema = new Schema({
  title: { type: String, required: true },
  url: { type: String, required: true, unique: true },
  description: { type: String, required: true },
  enable: { type: Boolean, default: false },
  body: { type: String, required: true },
  createdAt: { type: Date, default: Date.now },
});

export default mongoose.model('Post', PostShema);
