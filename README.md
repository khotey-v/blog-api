## Clone it
```sh
git clone https://github.com/khotey/express-starter
cd express-starter
```

## Make it your own
```sh
rm -rf .git && git init && npm init
```

## Install dependencies
```sh
yarn install
```

Description
---------------

```sh
|- /app
|  |- /controllers   - controllers
|  |- /models        - models
|  |- /config        - configurations
|  |- /routes        - routes
|  |- /middlewares   - middlewares
|  |- /tests         - tests dir
|  |- main.js        - main file of app
|  |- app.js         - bootstrap of app
|  |- server.js      - http server
```

Usage
---------------

```sh
# Run server in development mode
yarn run dev:run

# Build node server for production
yarn run prod:build

# Run node server for production after build
yarn run prod:run
```